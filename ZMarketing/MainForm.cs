﻿using Facebook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Timers;

namespace ZMarketing
{
    public partial class MainForm : Form
    {
        protected enum ACTION_STATE
        {
            NONE,
            READY,
            RUNNING,
            STOPPING
        }
        protected enum REQUEST_STATE
        {
            NONE,
            GET_MY_GROUPS,
            GET_FRIENDS_GROUPS,
            GET_FANPAGE,

            JOIN_GROUP
        }
        protected struct GroupInfo
        {
            public string id;
            public string name;
            public string privacy;
            public int memCount;
        }
        static string m_strFBHome = "https://www.facebook.com/";
        static string m_strAppName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        static string m_strAppVer = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

        protected static System.Timers.Timer m_tmJoinGroup = new System.Timers.Timer();
        protected static int m_nNextJoinGroupIndex = 0;
        protected static int m_nCurrentJoinGroupIndex = 0;

        protected string client_id;
        protected string client_secret;
        protected string redirect_uri;
        protected string email;
        protected string pass;
        protected string user_id;
        protected string user_name;

        protected FacebookClient fb;
        protected FacebookClient fbApp;
        protected string access_token;
        protected string access_token_app;

        protected REQUEST_STATE m_requestState;
        protected List<GroupInfo> m_lsMyGroups;

        // Searching group
        WebBrowser m_webSearchGroupTask;
        protected ACTION_STATE m_searchingState;
        WebBrowser m_webJoinGroup;
        public MainForm()
        {
            InitializeComponent();
            InitGroupTask();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            // Init program info
            lbAppName.Text = m_strAppName;
            // Init formation
            email = "issgjzx_martinazziescu_1444394584@tfbnw.net";
            pass = "123456ab";

            // namit3@gmail.com
            client_id = "741804302598376";
            client_secret = "5f27f017f4efda624db9e14110b238a6";
            redirect_uri = "https://apps.facebook.com/zmarketingvn";

            // thanhnam_a7@yahoo.com
            //client_id = "1648205862088978";
            //client_secret = "d5ce690d8ff667dc9eac6e00e37aeb49";
            //redirect_uri = "https://apps.facebook.com/zmarketinghd";

            user_id = "";

            txtEmail.Text = email;
            txtPass.Text = pass;

            m_webBroswer.Hide();
            //m_webBroswer.Size = new Size(800, 800);
            //m_webBroswer.Top = 100;
            //m_webBroswer.Left = 500;
            //this.Controls.Add(m_webBroswer);
            //this.Controls.SetChildIndex(m_webBroswer, 0);
            m_lsMyGroups = new List<GroupInfo>();

            fb = new FacebookClient();
            fbApp = new FacebookClient();

            m_requestState = REQUEST_STATE.NONE;
        }

        protected void InitGroupTask()
        {
            m_webSearchGroupTask = new WebBrowser();
            m_webJoinGroup = new WebBrowser();
            m_webJoinGroup.ScriptErrorsSuppressed = true;
            m_webSearchGroupTask.ScriptErrorsSuppressed = true;
            m_searchingState = ACTION_STATE.READY;

            m_webJoinGroup.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(m_webJoinGroupTask_DocumentCompleted);

            m_tmJoinGroup.Elapsed += OnTimedEvent;
            m_tmJoinGroup.AutoReset = true;
        }

        protected void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            System.Timers.Timer tm = source as System.Timers.Timer;
            if (tm == m_tmJoinGroup)
            {
                BeginInvoke((Action)delegate()
                {
                    // Find next index to join in next event
                    bool bNext = false;
                    if (m_nNextJoinGroupIndex >= 0 && m_nNextJoinGroupIndex < m_lvJoinGroup.Items.Count)
                    {
                        m_webJoinGroup.Navigate(m_lvJoinGroup.Items[m_nNextJoinGroupIndex].SubItems[5].Text);
                        m_nCurrentJoinGroupIndex = m_nNextJoinGroupIndex;
                    }
                    m_nNextJoinGroupIndex++;
                    for (int i = m_nNextJoinGroupIndex; i < m_lvJoinGroup.Items.Count; i++)
                    {
                        ListViewItem lvItem = m_lvJoinGroup.Items[i];
                        if (lvItem.Checked)
                        {
                            m_nNextJoinGroupIndex = i;
                            bNext = true;
                            break;
                        }
                    }
                    if (!bNext)
                    {
                        m_tmJoinGroup.Stop();
                        btnStartJoinGroup.Enabled = true;
                        btnStopJoinGroup.Enabled = false;
                        return;
                    }
                });
                /*
                BeginInvoke((Action)delegate()
                {
                    string preText = m_webGroupTask.DocumentText;
                    bool bNext = false;
                    if (m_nNextJoinGroupIndex >= 0 && m_nNextJoinGroupIndex < m_lvJoinGroup.Items.Count)
                    {
                        // Join group
                        ListViewItem lvi = m_lvJoinGroup.Items[m_nNextJoinGroupIndex];
                        string id = lvi.SubItems[3].Text;
                        var links = m_webGroupTask.Document.GetElementsByTagName("a");
                        foreach (HtmlElement link in links)
                        {
                            if (link != null && link.InnerText != null && link.GetAttribute("ajaxify") != null
                                && link.InnerText.Contains("Join") && link.GetAttribute("ajaxify").Contains(id))
                            {
                                link.InvokeMember("click");
                                lvi.SubItems[6].Text = "Đã tham gia";
                                lvi.BackColor = Color.YellowGreen;
                                break;
                            }
                        }
                        if (string.IsNullOrEmpty(lvi.SubItems[6].Text))
                        {
                            lvi.BackColor = Color.Red;
                            lvi.SubItems[6].Text = "Lỗi";
                        }
                        // Find next index to join in next event
                        m_nNextJoinGroupIndex++;
                        for (int i = m_nNextJoinGroupIndex; i < m_lvJoinGroup.Items.Count; i++)
                        {
                            ListViewItem lvItem = m_lvJoinGroup.Items[i];
                            if (lvItem.Checked)
                            {
                                m_nNextJoinGroupIndex = i;
                                bNext = true;
                                break;
                            }
                        }
                    }
                    if (!bNext)
                    {
                        m_tmJoinGroup.Stop();
                        btnStartJoinGroup.Enabled = true;
                        btnStopJoinGroup.Enabled = false;
                    }
                    m_webGroupTask.DocumentText = preText;
                });
                 */
            }
        }
        private void GetAppAccessToken()
        {
            var url = string.Format("/oauth/access_token?client_id={0}&client_secret={1}&grant_type=client_credentials", client_id, client_secret);
            dynamic result = fbApp.Get(url);
            string str = result.access_token;
            fbApp.AccessToken = access_token_app = str.Substring(str.IndexOf("|") + 1);
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            m_webBroswer.Show();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = client_id,
                client_secret = client_secret,
                redirect_uri = redirect_uri,
                response_type = "code token",
                scope = "user_about_me, user_managed_groups, manage_pages, publish_pages, user_posts, read_custom_friendlists, publish_actions, user_managed_groups" // Add other permissions as needed
            });
            m_webBroswer.Navigate(loginUrl);
            m_webBroswer.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(m_webBroswer_DocumentCompleted);
            m_webSearchGroupTask.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(m_webSearchGroupTask_DocumentCompleted);
        }
        void m_webJoinGroupTask_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (m_nCurrentJoinGroupIndex >= 0 && m_nCurrentJoinGroupIndex < m_lvJoinGroup.Items.Count)
            {
                BeginInvoke((Action)delegate()
                {
                    // Join group
                    ListViewItem lvi = m_lvJoinGroup.Items[m_nCurrentJoinGroupIndex];
                    string id = lvi.SubItems[3].Text;
                    var links = m_webJoinGroup.Document.GetElementsByTagName("a");
                    foreach (HtmlElement link in links)
                    {
                        if (link.InnerText == "Join Group")
                        {
                            link.InvokeMember("click");
                            m_lvJoinGroup.Items[m_nCurrentJoinGroupIndex].SubItems[6].Text = "Đã tham gia";
                            m_lvJoinGroup.Items[m_nCurrentJoinGroupIndex].BackColor = Color.YellowGreen;
                            break;
                        }
                    }
                    if (string.IsNullOrEmpty(lvi.SubItems[6].Text))
                    {
                        m_lvJoinGroup.Items[m_nCurrentJoinGroupIndex].BackColor = Color.Red;
                        m_lvJoinGroup.Items[m_nCurrentJoinGroupIndex].SubItems[6].Text = "Lỗi";
                    }
                });
            }
        }
        void m_webBroswer_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            string url = m_webBroswer.Url.ToString();
            if (url.Contains("access_token") && string.IsNullOrEmpty(access_token))
            {
                fb.AccessToken = access_token = url.Substring(url.IndexOf("#") + 14);

                // Get App access token
                GetAppAccessToken();

                // Load picture profile
                picAvatar.ImageLocation = (new Uri(string.Format("https://graph.facebook.com/me/picture?type={0}&access_token={1}", "large", fb.AccessToken))).ToString();

                //// Load Name
                dynamic result = fb.Get("me?fields=id, name");
                lbName.Text = user_name = result.name;
                user_id = result.id;

                // All done
                m_webBroswer.Hide();
                MessageBox.Show("Đăng nhập thành công!", m_strAppName);
            }
            if (m_requestState == REQUEST_STATE.JOIN_GROUP)
            {
                string result = m_webBroswer.DocumentText;
                var links = m_webBroswer.Document.GetElementsByTagName("a");
                foreach (HtmlElement link in links)
                {
                    if (link.InnerText == "Join Group")
                    {
                        link.InvokeMember("click");
                    }
                }
            }
            if (m_requestState == REQUEST_STATE.GET_MY_GROUPS && m_lsMyGroups.Count == 0)
            {
                string result = m_webBroswer.DocumentText;
                List<string> lsIds = new List<string>();

                string parten = @"(data-hovercard=\""/ajax/hovercard/group.php\??id=)(\w+\"">)([^<]*)";
                Regex rgx = new Regex(parten);
                MatchCollection matches = rgx.Matches(result);
                int nGroupCount = 0;
                foreach (Match match in matches)
                {
                    string strTmp = match.Value;
                    string subGroup = "group.php?id=";
                    int nBeginId = strTmp.IndexOf(subGroup) + subGroup.Count();
                    int nEndId = strTmp.IndexOf("\">", nBeginId);
                    string id = strTmp.Substring(nBeginId, nEndId - nBeginId);

                    int nBeginName = strTmp.IndexOf(">") + 1;
                    if (nBeginName >= 0)
                    {
                        string name = strTmp.Substring(nBeginName);
                        name = Utility.GetStringWithoutLineBreak(name);

                        nGroupCount++;
                        GroupInfo group = new GroupInfo();
                        group.id = id;
                        group.name = name;
                        m_lsMyGroups.Add(group);

                        ListViewItem lvi = new ListViewItem();
                        lvi.Checked = true;
                        lvi.SubItems.Add(nGroupCount.ToString());
                        lvi.SubItems.Add(group.name);
                        m_lvLoginGroup.Items.Add(lvi);
                    }
                }

                /* Request to get groups info
                string parten1 = @"(aria-hidden=\""true\"" href=\""/groups/)(\S)+";
                Regex rgx1 = new Regex(parten1);
                MatchCollection matches1 = rgx1.Matches(result);
                foreach (Match match in matches1)
                {
                    string strTmp = match.Value;
                    string subGroup = "/groups/";
                    int nBeginId = strTmp.IndexOf(subGroup) + subGroup.Count();
                    int nEndId = strTmp.IndexOf("/", nBeginId);
                    string id = strTmp.Substring(nBeginId, nEndId - nBeginId);
                    if (!lsIds.Contains(id))
                        lsIds.Add(id);
                }

                string parten2 = @"(data-hovercard=\""/ajax/hovercard/group.php\??id=)(\w+)";
                Regex rgx2 = new Regex(parten2);
                MatchCollection matches2 = rgx2.Matches(result);
                foreach (Match match in matches2)
                {
                    string strTmp = match.Value;
                    string subGroup = "group.php?id=";
                    string id = strTmp.Substring(strTmp.IndexOf(subGroup) + subGroup.Count());
                    if (!lsIds.Contains(id))
                        lsIds.Add(id);
                }
                int nGroupCount = 0;
                foreach (string id in lsIds)
                {
                    try
                    {
                        dynamic rs = fb.Get("/" + id);
                        nGroupCount++;
                        GroupInfo group = new GroupInfo();
                        group.id = rs.id;
                        group.name = rs.name;
                        m_lsMyGroups.Add(group);

                        ListViewItem lvi = new ListViewItem();
                        lvi.Checked = true;
                        lvi.SubItems.Add(nGroupCount.ToString());
                        lvi.SubItems.Add(group.name);
                        m_lvLoginGroup.Items.Add(lvi);
                    }
                    catch
                    {

                    }
                }*/
            }
        }
        void m_webSearchGroupTask_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (m_searchingState != ACTION_STATE.RUNNING)
            {
                m_webSearchGroupTask.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(m_webSearchGroupTask_DocumentCompleted);
                return;
            }

            string result = m_webSearchGroupTask.DocumentText;
            // Finding groups
            string parten = @"(group_id=)([\s\S]*?(?= members<))";
            Regex rgx = new Regex(parten);
            MatchCollection matches = rgx.Matches(result);
            if (matches.Count == 0)
            {
                // Not found any group, end searching group
                m_searchingState = ACTION_STATE.READY;
                MessageBox.Show("Không tìm thấy group", m_strAppName);
            }
            int nGroupCount = m_lvJoinGroup.Items.Count;
            foreach (Match match in matches)
            {
                string strValue = match.Value;
                string strValueLower = match.Value.ToLower();
                string subGroup = "group_id=";
                int nBeginId = strValueLower.IndexOf(subGroup) + subGroup.Count();
                int nEndId = strValueLower.IndexOf("\"", nBeginId);
                string id = strValue.Substring(nBeginId, nEndId - nBeginId);

                int nBeginName = strValueLower.IndexOf("<a href=\"/groups/");
                if (nBeginName >= 0)
                {
                    if (nGroupCount >= Decimal.ToInt32(nbGroups.Value))
                    {
                        // Stop searching
                        m_searchingState = ACTION_STATE.READY;
                        return;
                    }
                    nBeginName = strValueLower.IndexOf(">", nBeginName) + 1;
                    int nEndName = strValueLower.IndexOf("</a>", nBeginName);
                    string name = strValue.Substring(nBeginName, nEndName - nBeginName);
                    name = Utility.GetStringWithoutLineBreak(name);
                    name = System.Net.WebUtility.HtmlDecode(name);

                    // Check duplicate group
                    bool bDup = false;
                    foreach (ListViewItem lvi in m_lvJoinGroup.Items)
                    {
                        if (lvi.SubItems[3].Text == id)
                        {
                            bDup = true;
                            break;
                        }
                    }
                    if (!bDup)
                    {
                        string sCount = strValue.Substring(strValue.LastIndexOf(">") + 1);
                        GroupInfo group = new GroupInfo();
                        group.id = id;
                        group.name = name;
                        group.memCount = Int32.Parse(sCount, System.Globalization.NumberStyles.Any);

                        if (group.memCount < Decimal.ToInt32(nbGreater.Value)
                            || group.memCount > Decimal.ToInt32(nbLessThan.Value))
                            continue;

                        ListViewItem lvi = new ListViewItem();
                        lvi.Checked = true;
                        lvi.SubItems.Add((nGroupCount + 1).ToString());
                        lvi.SubItems.Add(group.name);
                        lvi.SubItems.Add(id);
                        lvi.SubItems.Add(sCount);
                        lvi.SubItems.Add(m_strFBHome + id);
                        lvi.SubItems.Add("");
                        m_lvJoinGroup.Items.Add(lvi);
                        nGroupCount++;
                    }
                }
            }
            m_webSearchGroupTask.Document.Window.ScrollTo(0, m_webSearchGroupTask.Document.Body.ScrollRectangle.Height);
        }
        private void btnGetGroup_Click(object sender, EventArgs e)
        {
            m_lsMyGroups.Clear();
            m_lvLoginGroup.Items.Clear();
            string url = "https://www.facebook.com/groups/?category=membership";
            m_requestState = REQUEST_STATE.GET_MY_GROUPS;
            m_webBroswer.Navigate(url);
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            m_webBroswer.Show();
            var logoutParameters = new Dictionary<string, object>
                  {
                      { "next", "http://www.facebook.com" },
                      { "access_token", fb.AccessToken }
                  };
            var logoutUrl = fb.GetLogoutUrl(logoutParameters);
            m_webBroswer.Navigate(logoutUrl.AbsoluteUri);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtGroupSearchKeyword.Text))
            {
                MessageBox.Show("Nhập từ khóa để tìm kiếm.", m_strAppName);
                return;
            }
            if (m_searchingState == ACTION_STATE.RUNNING)
                return;
            m_webSearchGroupTask.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(m_webSearchGroupTask_DocumentCompleted);

            btnSearchGroup.Enabled = false;
            btnStopSearchGroup.Enabled = true;

            m_searchingState = ACTION_STATE.RUNNING;
            m_lvJoinGroup.Items.Clear();
            string url = string.Format("https://www.facebook.com/search/str/{0}/keywords_groups", HttpUtility.UrlEncode(txtGroupSearchKeyword.Text));
            m_webSearchGroupTask.Navigate(url);

            m_nNextJoinGroupIndex = 0;
            /*
            // Start searching groups
            m_lvJoinGroup.Items.Clear();
            int nGroupCount = 0;
            string url = string.Format("search?q={0}&type=group", txtGroupSearchKeyword.Text);
            while(true)
            {
                // Search groups
                dynamic result = fb.Get(url);
                foreach (dynamic item in result.data)
                {
                    string name = item.name;
                    string id = item.id;
                    string privacy = item.privacy;

                    // Get group info
                    nGroupCount++;
                    ListViewItem lvi = new ListViewItem();
                    string link = "https://www.facebook.com/" + id;
                    lvi.Checked = true;
                    lvi.SubItems.Add(nGroupCount.ToString());
                    lvi.SubItems.Add(name);
                    lvi.SubItems.Add("");
                    lvi.SubItems.Add(link);
                    lvi.SubItems.Add("");
                    m_lvJoinGroup.Items.Add(lvi);

                    Int64 memCount = 0;
                    m_webBroswer.Navigate(link);
                    while (true)
                    {
                        if (m_webBroswer.ReadyState == WebBrowserReadyState.Complete)
                        {
                            var spans = m_webBroswer.Document.GetElementsByTagName("span");
                            foreach (HtmlElement span in spans)
                            {
                                if (span.InnerText.Contains("Member"))
                                {
                                    string numStr = span.InnerText.Substring(0, span.InnerText.IndexOf(" "));
                                    if (long.TryParse(lvi.SubItems[3].Text, out memCount))
                                    {
                                        lvi.SubItems[3].Text = numStr;
                                    }

                                }
                            }
                            break;
                        }
                        else
                            System.Threading.Thread.Sleep(300);
                    }

                    if (nGroupCount == nbGroups.Value)
                        return;
                }
                url = result.paging.next;
                if (string.IsNullOrEmpty(url))
                    break;
            }
            */
        }

        private void btnJoinGroup_Click(object sender, EventArgs e)
        {
            //foreach(ListViewItem lvi in m_lvJoinGroup.Items)
            //{
            //    if (lvi.Checked)
            //    {
            //        string id = lvi.SubItems[3].Text;
            //        var links = m_webGroupTask.Document.GetElementsByTagName("a");
            //        foreach (HtmlElement link in links)
            //        {
            //            if (link != null && link.InnerText != null && link.GetAttribute("ajaxify") != null
            //                && link.InnerText.Contains("Join") && link.GetAttribute("ajaxify").Contains(id))
            //            {
            //                link.InvokeMember("click");
            //            }
            //        }
            //    }
            //}

            // Find next index to join in next event
            bool bStartFind = false;
            for (int i = m_nNextJoinGroupIndex; i < m_lvJoinGroup.Items.Count; i++)
            {
                ListViewItem lvItem = m_lvJoinGroup.Items[i];
                if (lvItem.Checked)
                {
                    m_nNextJoinGroupIndex = i;
                    bStartFind = true;
                    break;
                }
            }
            if (bStartFind)
            {
                btnStartJoinGroup.Enabled = false;
                btnStopJoinGroup.Enabled = true;

                m_tmJoinGroup.Interval = 1000 * Decimal.ToInt32(nbJoinInterval.Value);
                m_tmJoinGroup.Start();
            }
        }

        private void lvJoinGroup_MouseMove(object sender, MouseEventArgs e)
        {
            var hit = m_lvJoinGroup.HitTest(e.Location);
            if (hit.SubItem != null && hit.SubItem == hit.Item.SubItems[5]) m_lvJoinGroup.Cursor = Cursors.Hand;
            else m_lvJoinGroup.Cursor = Cursors.Default;
        }

        private void lvJoinGroup_MouseUp(object sender, MouseEventArgs e)
        {
            var hit = m_lvJoinGroup.HitTest(e.Location);
            if (hit.SubItem != null && hit.SubItem == hit.Item.SubItems[5])
            {
                var url = new Uri(hit.SubItem.Text);
                System.Diagnostics.Process.Start(url.ToString());
            }
        }

        private void fbBtn_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.facebook.com");
        }

        private void ytBtn_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.youtube.com");
        }

        private void gBtn_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.google.com");
        }

        private void btnStopSearchGroup_Click(object sender, EventArgs e)
        {
            m_searchingState = ACTION_STATE.READY;
            m_webSearchGroupTask.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(m_webSearchGroupTask_DocumentCompleted);
            btnSearchGroup.Enabled = true;
            btnStopSearchGroup.Enabled = false;
        }

        private void btnStopJoinGroup_Click(object sender, EventArgs e)
        {
            btnStartJoinGroup.Enabled = true;
            btnStopJoinGroup.Enabled = false;
            m_tmJoinGroup.Stop();
        }
    }
}
