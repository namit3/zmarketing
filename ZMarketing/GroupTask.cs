﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ZMarketing
{
    class GroupTask
    {
        // For searching group
        protected int m_nMinMem;
        protected int m_nMaxMem;
        protected string m_sKeyword;
        protected int m_nGroupLimit;
        protected string m_sFriendId;
        protected int m_nJoiningInterval; // Period time between 2 joining

        static public ListView m_lvGroup = new ListView();
        static public WebBrowser m_webBroswer = new WebBrowser();
        public int MinMem { get {return m_nMinMem;} set {m_nMinMem = value;} }
        public int MaxMem { get { return m_nMaxMem; } set { m_nMaxMem = value; } }
        public string Keyword { get { return m_sKeyword; } set { m_sKeyword = value; } }
        public int GroupLimit { get { return m_nGroupLimit; } set { m_nGroupLimit = value; } }
        public string FriendId { get { return m_sFriendId; } set { m_sFriendId = value; } }
        public int JoiningInterval { get { return m_nJoiningInterval; } set { m_nJoiningInterval = value; } }
        public WebBrowser WebCtrl { get { return m_webBroswer; } set { m_webBroswer = value; } }
        public void SetWeb(ref WebBrowser web)
        {
            m_webBroswer = web;
        }

        public static void SearchGroup()
        {
            int i = 0;
            while (true)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Checked = true;
                lvi.SubItems.Add(i.ToString());
                lvi.SubItems.Add("");
                lvi.SubItems.Add("");
                lvi.SubItems.Add("");
                lvi.SubItems.Add("");
                m_lvGroup.Items.Add(lvi);
                i++;
                Thread.Sleep(100);
            }
        }

    }
}
