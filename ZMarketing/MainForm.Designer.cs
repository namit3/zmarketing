﻿namespace ZMarketing
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabLogin = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbAppName = new System.Windows.Forms.Label();
            this.m_webBroswer = new System.Windows.Forms.WebBrowser();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.m_lvLoginGroup = new System.Windows.Forms.ListView();
            this.sel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.index = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnExportGroup = new System.Windows.Forms.Button();
            this.btnGroupFromFile = new System.Windows.Forms.Button();
            this.btnDelGroup = new System.Windows.Forms.Button();
            this.btnGetGroup = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnLogout = new System.Windows.Forms.Button();
            this.lbName = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabGroup = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabJoinGroup = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnStopJoinGroup = new System.Windows.Forms.Button();
            this.btnStartJoinGroup = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.nbJoinInterval = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.m_lvJoinGroup = new System.Windows.Forms.ListView();
            this.clCheck = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clSTT = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clMemCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clLink = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnStopSearchGroup = new System.Windows.Forms.Button();
            this.nbGroups = new System.Windows.Forms.NumericUpDown();
            this.nbGreater = new System.Windows.Forms.NumericUpDown();
            this.nbLessThan = new System.Windows.Forms.NumericUpDown();
            this.btnSearchGroup = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtGroupSearchKeyword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabInviteFriendToGroup = new System.Windows.Forms.TabPage();
            this.tabLeaveGroup = new System.Windows.Forms.TabPage();
            this.tabGroupManage = new System.Windows.Forms.TabPage();
            this.tabFpMark = new System.Windows.Forms.TabPage();
            this.tabPosts = new System.Windows.Forms.TabPage();
            this.fbBtn = new System.Windows.Forms.PictureBox();
            this.gBtn = new System.Windows.Forms.PictureBox();
            this.ytBtn = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picAvatar = new System.Windows.Forms.PictureBox();
            this.tabMain.SuspendLayout();
            this.tabLogin.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabGroup.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabJoinGroup.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbJoinInterval)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbGreater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbLessThan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fbBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ytBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAvatar)).BeginInit();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            resources.ApplyResources(this.tabMain, "tabMain");
            this.tabMain.Controls.Add(this.tabLogin);
            this.tabMain.Controls.Add(this.tabGroup);
            this.tabMain.Controls.Add(this.tabFpMark);
            this.tabMain.Controls.Add(this.tabPosts);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            // 
            // tabLogin
            // 
            this.tabLogin.Controls.Add(this.groupBox3);
            this.tabLogin.Controls.Add(this.m_webBroswer);
            this.tabLogin.Controls.Add(this.groupBox2);
            this.tabLogin.Controls.Add(this.groupBox1);
            resources.ApplyResources(this.tabLogin, "tabLogin");
            this.tabLogin.Name = "tabLogin";
            this.tabLogin.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.linkLabel1);
            this.groupBox3.Controls.Add(this.fbBtn);
            this.groupBox3.Controls.Add(this.gBtn);
            this.groupBox3.Controls.Add(this.ytBtn);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.lbAppName);
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // linkLabel1
            // 
            resources.ApplyResources(this.linkLabel1, "linkLabel1");
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.TabStop = true;
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.Name = "label24";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // lbAppName
            // 
            resources.ApplyResources(this.lbAppName, "lbAppName");
            this.lbAppName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbAppName.Name = "lbAppName";
            // 
            // m_webBroswer
            // 
            resources.ApplyResources(this.m_webBroswer, "m_webBroswer");
            this.m_webBroswer.Name = "m_webBroswer";
            this.m_webBroswer.Url = new System.Uri("https://facebook.com", System.UriKind.Absolute);
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Controls.Add(this.m_lvLoginGroup);
            this.groupBox2.Controls.Add(this.btnExportGroup);
            this.groupBox2.Controls.Add(this.btnGroupFromFile);
            this.groupBox2.Controls.Add(this.btnDelGroup);
            this.groupBox2.Controls.Add(this.btnGetGroup);
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // m_lvLoginGroup
            // 
            resources.ApplyResources(this.m_lvLoginGroup, "m_lvLoginGroup");
            this.m_lvLoginGroup.CheckBoxes = true;
            this.m_lvLoginGroup.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.sel,
            this.index,
            this.name});
            this.m_lvLoginGroup.FullRowSelect = true;
            this.m_lvLoginGroup.Name = "m_lvLoginGroup";
            this.m_lvLoginGroup.UseCompatibleStateImageBehavior = false;
            this.m_lvLoginGroup.View = System.Windows.Forms.View.Details;
            // 
            // sel
            // 
            resources.ApplyResources(this.sel, "sel");
            // 
            // index
            // 
            resources.ApplyResources(this.index, "index");
            // 
            // name
            // 
            resources.ApplyResources(this.name, "name");
            // 
            // btnExportGroup
            // 
            resources.ApplyResources(this.btnExportGroup, "btnExportGroup");
            this.btnExportGroup.Name = "btnExportGroup";
            this.btnExportGroup.UseVisualStyleBackColor = true;
            // 
            // btnGroupFromFile
            // 
            resources.ApplyResources(this.btnGroupFromFile, "btnGroupFromFile");
            this.btnGroupFromFile.Name = "btnGroupFromFile";
            this.btnGroupFromFile.UseVisualStyleBackColor = true;
            // 
            // btnDelGroup
            // 
            resources.ApplyResources(this.btnDelGroup, "btnDelGroup");
            this.btnDelGroup.Name = "btnDelGroup";
            this.btnDelGroup.UseVisualStyleBackColor = true;
            // 
            // btnGetGroup
            // 
            resources.ApplyResources(this.btnGetGroup, "btnGetGroup");
            this.btnGetGroup.Name = "btnGetGroup";
            this.btnGetGroup.UseVisualStyleBackColor = true;
            this.btnGetGroup.Click += new System.EventHandler(this.btnGetGroup_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnLogout);
            this.groupBox1.Controls.Add(this.lbName);
            this.groupBox1.Controls.Add(this.btnLogin);
            this.groupBox1.Controls.Add(this.txtPass);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.picAvatar);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // btnLogout
            // 
            resources.ApplyResources(this.btnLogout, "btnLogout");
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // lbName
            // 
            resources.ApplyResources(this.lbName, "lbName");
            this.lbName.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbName.Name = "lbName";
            // 
            // btnLogin
            // 
            resources.ApplyResources(this.btnLogin, "btnLogin");
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtPass
            // 
            resources.ApplyResources(this.txtPass, "txtPass");
            this.txtPass.Name = "txtPass";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // txtEmail
            // 
            resources.ApplyResources(this.txtEmail, "txtEmail");
            this.txtEmail.Name = "txtEmail";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Name = "label1";
            // 
            // tabGroup
            // 
            this.tabGroup.Controls.Add(this.tabControl1);
            resources.ApplyResources(this.tabGroup, "tabGroup");
            this.tabGroup.Name = "tabGroup";
            this.tabGroup.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Controls.Add(this.tabJoinGroup);
            this.tabControl1.Controls.Add(this.tabInviteFriendToGroup);
            this.tabControl1.Controls.Add(this.tabLeaveGroup);
            this.tabControl1.Controls.Add(this.tabGroupManage);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            // 
            // tabJoinGroup
            // 
            this.tabJoinGroup.Controls.Add(this.groupBox6);
            this.tabJoinGroup.Controls.Add(this.m_lvJoinGroup);
            this.tabJoinGroup.Controls.Add(this.groupBox5);
            resources.ApplyResources(this.tabJoinGroup, "tabJoinGroup");
            this.tabJoinGroup.Name = "tabJoinGroup";
            this.tabJoinGroup.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            resources.ApplyResources(this.groupBox6, "groupBox6");
            this.groupBox6.Controls.Add(this.btnStopJoinGroup);
            this.groupBox6.Controls.Add(this.btnStartJoinGroup);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.nbJoinInterval);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.TabStop = false;
            // 
            // btnStopJoinGroup
            // 
            resources.ApplyResources(this.btnStopJoinGroup, "btnStopJoinGroup");
            this.btnStopJoinGroup.Name = "btnStopJoinGroup";
            this.btnStopJoinGroup.UseVisualStyleBackColor = true;
            this.btnStopJoinGroup.Click += new System.EventHandler(this.btnStopJoinGroup_Click);
            // 
            // btnStartJoinGroup
            // 
            resources.ApplyResources(this.btnStartJoinGroup, "btnStartJoinGroup");
            this.btnStartJoinGroup.Name = "btnStartJoinGroup";
            this.btnStartJoinGroup.UseVisualStyleBackColor = true;
            this.btnStartJoinGroup.Click += new System.EventHandler(this.btnJoinGroup_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // nbJoinInterval
            // 
            resources.ApplyResources(this.nbJoinInterval, "nbJoinInterval");
            this.nbJoinInterval.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.nbJoinInterval.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nbJoinInterval.Name = "nbJoinInterval";
            this.nbJoinInterval.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // m_lvJoinGroup
            // 
            resources.ApplyResources(this.m_lvJoinGroup, "m_lvJoinGroup");
            this.m_lvJoinGroup.CheckBoxes = true;
            this.m_lvJoinGroup.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clCheck,
            this.clSTT,
            this.clName,
            this.clID,
            this.clMemCount,
            this.clLink,
            this.clStatus});
            this.m_lvJoinGroup.FullRowSelect = true;
            this.m_lvJoinGroup.GridLines = true;
            this.m_lvJoinGroup.LabelEdit = true;
            this.m_lvJoinGroup.Name = "m_lvJoinGroup";
            this.m_lvJoinGroup.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.m_lvJoinGroup.UseCompatibleStateImageBehavior = false;
            this.m_lvJoinGroup.View = System.Windows.Forms.View.Details;
            this.m_lvJoinGroup.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lvJoinGroup_MouseMove);
            this.m_lvJoinGroup.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lvJoinGroup_MouseUp);
            // 
            // clCheck
            // 
            resources.ApplyResources(this.clCheck, "clCheck");
            // 
            // clSTT
            // 
            resources.ApplyResources(this.clSTT, "clSTT");
            // 
            // clName
            // 
            resources.ApplyResources(this.clName, "clName");
            // 
            // clID
            // 
            resources.ApplyResources(this.clID, "clID");
            // 
            // clMemCount
            // 
            resources.ApplyResources(this.clMemCount, "clMemCount");
            // 
            // clLink
            // 
            resources.ApplyResources(this.clLink, "clLink");
            // 
            // clStatus
            // 
            resources.ApplyResources(this.clStatus, "clStatus");
            // 
            // groupBox5
            // 
            resources.ApplyResources(this.groupBox5, "groupBox5");
            this.groupBox5.Controls.Add(this.btnStopSearchGroup);
            this.groupBox5.Controls.Add(this.nbGroups);
            this.groupBox5.Controls.Add(this.nbGreater);
            this.groupBox5.Controls.Add(this.nbLessThan);
            this.groupBox5.Controls.Add(this.btnSearchGroup);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.txtGroupSearchKeyword);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.TabStop = false;
            // 
            // btnStopSearchGroup
            // 
            resources.ApplyResources(this.btnStopSearchGroup, "btnStopSearchGroup");
            this.btnStopSearchGroup.Name = "btnStopSearchGroup";
            this.btnStopSearchGroup.UseVisualStyleBackColor = true;
            this.btnStopSearchGroup.Click += new System.EventHandler(this.btnStopSearchGroup_Click);
            // 
            // nbGroups
            // 
            this.nbGroups.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            resources.ApplyResources(this.nbGroups, "nbGroups");
            this.nbGroups.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.nbGroups.Name = "nbGroups";
            this.nbGroups.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // nbGreater
            // 
            resources.ApplyResources(this.nbGreater, "nbGreater");
            this.nbGreater.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nbGreater.Name = "nbGreater";
            this.nbGreater.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // nbLessThan
            // 
            resources.ApplyResources(this.nbLessThan, "nbLessThan");
            this.nbLessThan.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nbLessThan.Name = "nbLessThan";
            this.nbLessThan.Value = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            // 
            // btnSearchGroup
            // 
            resources.ApplyResources(this.btnSearchGroup, "btnSearchGroup");
            this.btnSearchGroup.Name = "btnSearchGroup";
            this.btnSearchGroup.UseVisualStyleBackColor = true;
            this.btnSearchGroup.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // txtGroupSearchKeyword
            // 
            resources.ApplyResources(this.txtGroupSearchKeyword, "txtGroupSearchKeyword");
            this.txtGroupSearchKeyword.Name = "txtGroupSearchKeyword";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // tabInviteFriendToGroup
            // 
            resources.ApplyResources(this.tabInviteFriendToGroup, "tabInviteFriendToGroup");
            this.tabInviteFriendToGroup.Name = "tabInviteFriendToGroup";
            this.tabInviteFriendToGroup.UseVisualStyleBackColor = true;
            // 
            // tabLeaveGroup
            // 
            resources.ApplyResources(this.tabLeaveGroup, "tabLeaveGroup");
            this.tabLeaveGroup.Name = "tabLeaveGroup";
            this.tabLeaveGroup.UseVisualStyleBackColor = true;
            // 
            // tabGroupManage
            // 
            resources.ApplyResources(this.tabGroupManage, "tabGroupManage");
            this.tabGroupManage.Name = "tabGroupManage";
            this.tabGroupManage.UseVisualStyleBackColor = true;
            // 
            // tabFpMark
            // 
            resources.ApplyResources(this.tabFpMark, "tabFpMark");
            this.tabFpMark.Name = "tabFpMark";
            this.tabFpMark.UseVisualStyleBackColor = true;
            // 
            // tabPosts
            // 
            resources.ApplyResources(this.tabPosts, "tabPosts");
            this.tabPosts.Name = "tabPosts";
            this.tabPosts.UseVisualStyleBackColor = true;
            // 
            // fbBtn
            // 
            resources.ApplyResources(this.fbBtn, "fbBtn");
            this.fbBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fbBtn.Name = "fbBtn";
            this.fbBtn.TabStop = false;
            this.fbBtn.Click += new System.EventHandler(this.fbBtn_Click);
            // 
            // gBtn
            // 
            resources.ApplyResources(this.gBtn, "gBtn");
            this.gBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.gBtn.Name = "gBtn";
            this.gBtn.TabStop = false;
            this.gBtn.Click += new System.EventHandler(this.gBtn_Click);
            // 
            // ytBtn
            // 
            resources.ApplyResources(this.ytBtn, "ytBtn");
            this.ytBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ytBtn.Image = global::ZMarketing.Properties.Resources._1444606204_youtube_v2;
            this.ytBtn.Name = "ytBtn";
            this.ytBtn.TabStop = false;
            this.ytBtn.Click += new System.EventHandler(this.ytBtn_Click);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // picAvatar
            // 
            resources.ApplyResources(this.picAvatar, "picAvatar");
            this.picAvatar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picAvatar.Name = "picAvatar";
            this.picAvatar.TabStop = false;
            // 
            // MainForm
            // 
            this.AcceptButton = this.btnLogin;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabMain);
            this.Name = "MainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.OnLoad);
            this.tabMain.ResumeLayout(false);
            this.tabLogin.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabGroup.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabJoinGroup.ResumeLayout(false);
            this.tabJoinGroup.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbJoinInterval)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbGreater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbLessThan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fbBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ytBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAvatar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabLogin;
        private System.Windows.Forms.TabPage tabFpMark;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox picAvatar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnExportGroup;
        private System.Windows.Forms.Button btnGroupFromFile;
        private System.Windows.Forms.Button btnDelGroup;
        private System.Windows.Forms.Button btnGetGroup;
        private System.Windows.Forms.ListView m_lvLoginGroup;
        private System.Windows.Forms.ColumnHeader sel;
        private System.Windows.Forms.ColumnHeader index;
        private System.Windows.Forms.ColumnHeader name;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.TabPage tabGroup;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabJoinGroup;
        private System.Windows.Forms.TabPage tabInviteFriendToGroup;
        private System.Windows.Forms.TabPage tabLeaveGroup;
        private System.Windows.Forms.TabPage tabGroupManage;
        private System.Windows.Forms.ListView m_lvJoinGroup;
        private System.Windows.Forms.ColumnHeader clSTT;
        private System.Windows.Forms.ColumnHeader clID;
        private System.Windows.Forms.ColumnHeader clMemCount;
        private System.Windows.Forms.ColumnHeader clLink;
        private System.Windows.Forms.ColumnHeader clCheck;
        private System.Windows.Forms.ColumnHeader clName;
        private System.Windows.Forms.TabPage tabPosts;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnStartJoinGroup;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nbJoinInterval;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown nbGroups;
        private System.Windows.Forms.NumericUpDown nbGreater;
        private System.Windows.Forms.NumericUpDown nbLessThan;
        private System.Windows.Forms.Button btnSearchGroup;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtGroupSearchKeyword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.WebBrowser m_webBroswer;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbAppName;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox fbBtn;
        private System.Windows.Forms.PictureBox gBtn;
        private System.Windows.Forms.PictureBox ytBtn;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.ColumnHeader clStatus;
        private System.Windows.Forms.Button btnStopSearchGroup;
        private System.Windows.Forms.Button btnStopJoinGroup;


    }
}

