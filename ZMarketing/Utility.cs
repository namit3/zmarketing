﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ZMarketing
{
    class Utility
    {
        public static string GetStringWithoutLineBreak(string value)
        {
            value = Regex.Replace(value, @" \r\n | \n ", "");
            value = Regex.Replace(value, @" \r\n| \n", "");
            value = Regex.Replace(value, @"\r\n |\n ", "");
            value = Regex.Replace(value, @"\r\n?|\n", "");
            return value;
        }
    }
}
